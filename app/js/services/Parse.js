angular.module('app')
    .config(['$httpProvider', 'ParseAPIUrl', 'ParseAppID', 'ParseRESTKey', 'ParseJSKey', '$localStorageProvider', function($httpProvider, ParseAPIUrl, ParseAppID, ParseRESTKey, ParseJSKey, $storage) {
        window.fbAsyncInit = function() {
            Parse.FacebookUtils.init({
                appId: '506056806211603',
                status: true, // check Facebook Login status
                cookie: true, // enable cookies to allow Parse to access the session
                xfbml: true, // initialize Facebook social plugins on the page
                version: 'v2.5'
            });
        };
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        $httpProvider.interceptors.push(function($q, $location) {
            return {
                'request': function(config) {
                    // If the requesting url is RESTfull service then prefix the url with APIBaseURL
                    if (/^\/(classes|functions|login|logout|users)/.test(config.url)) {
                        config.url = ParseAPIUrl + config.url;
                        config.headers['X-Parse-Application-Id'] = ParseAppID;
                        config.headers['X-Parse-REST-API-Key'] = ParseRESTKey;
                        if (/^\/(login|users)/.test(config.url)) {
                            config.headers['X-Parse-Revocable-Session'] = "1";
                        }
                        try {
                            if ($storage.$get('user').user.sessionToken) {
                                config.headers['X-Parse-Session-Token'] = $storage.$get('user').user.sessionToken;
                            }
                        } catch (e) {
                            console.log('User not loggedin');
                        }
                    }
                    return config;
                }
            };
        });
    }])
    .service('User', ['$resource', function($resource) {
        return $resource('/users', {}, {
            signUp: {
                method: 'POST',
                url: '/users'
            },
            signIn: {
                method: 'GET',
                url: '/login'
            },
            details: {
                method: 'GET',
                url: '/users/me'
            },
            signOut: {
                method: 'POST',
                url: '/logout'
            }
        });
    }])
    .service('Universities', ['$resource', function($resource) {
        return $resource('/classes/UniversitiesData', {}, {
            getByID: {
                method: 'GET',
                url: '/classes/UniversitiesData/:id'
            },
            saveUniversity: {
                method: 'PUT',
                url: '/classes/UniversitiesData/:id'
            },
            getByNameLocationAndState: {
                method: 'GET',
                url: '/classes/UniversitiesData'
            }
        });
    }])
    .service('CloudAPI', ['$resource', function($resource) {
        return $resource('/functions/', {}, {
            SearchUniversities: {
                method: 'POST',
                url: '/functions/SearchUniversities'
            },
            UniversitiesList: {
                method: 'POST',
                url: '/functions/UniversitiesList'
            },
            AddToFavourite: {
                method: 'POST',
                url: '/functions/AddToFavourite'
            },
            GetFavUniversities: {
                method: 'POST',
                url: '/functions/GetFavUniversities'
            },
            GetUserDetails: {
                method: 'POST',
                url: '/functions/GetUserDetails'
            },
            SOPRequest: {
                method: 'POST',
                url: '/functions/SOPRequest'
            },
        });
    }]);