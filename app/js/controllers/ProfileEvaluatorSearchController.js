angular.module('app')
    .controller('ProfileEvaluatorSearchController', ['$scope', '$state', '$localStorage', 'User', '$timeout', 'searchParams', 'IP', function ($scope, $state, $storage, User, $timeout, searchParams, IP) {
        $scope.IP = IP.data;
        $scope.name = 'Home';
        score_type = 'TOEFL';
        $scope.sign_in_up = 'signin';
        
        $scope.ms_courses = ["CS", "EE", "MIS", "CE", "MECH", "IndustrialEng", "Civil", "MBA", "AE", "AE&MEC", "Arch", "MArch", "BA", "Biology", "Bioengineering", "BiologicalSciences", "BilogicalEng", "BiosystemsEng", "BiomedicalSciences", "BiomedicalEng", "Business Analytics", "ChemicalEngg", "Chemical&BiologicalEng", "Chemistry", "Biochemistry", "Chemistry&BioChemistry", "CS&E", "CIS", "CN", "Civil&EvnEng", "CM", "CE/CEM", "ECE", "EngMgt", "EnvEng", "ES/ESM", "Engineerg", "HA", "HCA", "IT/ITM", "INST", "IS", "INS", "IST", "INSA", "IS&M", "Industrial& SysEng", "MaterialsScience", "MaterialsEng", "Materials Science&Eng", "MML", "ManufEng/MSE", "Pharmacy", "PharmaScience", "PE", "Phy", "PH", "PM", "PA", "SE", "StructuralEng", "SystemsEng", "MPA", "MPH", "MHA", "MHCA"];
        $scope.states = ["KENTUCKY","ILLINOIS","OKLAHOMA","MARYLAND","WEST VIRGINIA","NORTH CAROLINA","LOUISIANA","VIRGINIA","CALIFORNIA","NEW MEXICO","WISCONSIN","INDIANA","CONNECTICUT","MICHIGAN","ALABAMA","IOWA","PENNSYLVANIA","NEW YORK","MASSACHUSETTS","COLORADO","TENNESSEE","TEXAS","NEW JERSEY","OREGON","FLORIDA","WASHINGTON","OHIO","ARKANSAS","GEORGIA","MISSISSIPPI","MISSOURI","NEW HAMPSHIRE","UTAH","ARIZONA","KANSAS","MONTANA","MINNESOTA","DELAWARE","IDAHO"];
        
        if ($storage.user && $storage.user.emailVerified !== undefined) {
            $scope.userEmail = $storage.user.email;
            $scope.showVerifyEmail = !$storage.user.emailVerified;
        }
        $scope.search = searchParams;
        $scope.searchDB = function (searchParams) {
            searchParams.gpa = (searchParams.percentage / 100 * 4).toFixed(2);
            searchParams.IP = $scope.IP.query;
            searchParams.city = JSON.stringify($scope.IP);
            $storage.searchParams = searchParams;
            $state.go('searchdb', {
                searchParams: searchParams
            });
        };
    }]);