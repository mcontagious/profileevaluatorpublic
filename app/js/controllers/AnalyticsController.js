var app = angular.module('app');
var requestData = {
    "deadLine": "NextFall",
    "course": "MS",
    "msMajor": "CS",
    "greScore": 320,
    "exam": "TOEFL",
    "examScore": 120,
    "Backlogs": 0,
    "percentage": 78,
    "cgpa": 3.12,
    "gpa": "3.12"

};

var graphData = [];
app.controller('analyticsController', ['$scope', '$http', 'ParseAPIUrl', function($scope, $http, ParseAPIUrl) {

    /**
     * Ms program courses.
     **/
    $scope.ms_courses = ["CS", "EE", "MIS", "CE", "MECH", "IndustrialEng", "Civil", "MBA", "AE", "AE&MEC", "Arch", "MArch", "BA", "Biology", "Bioengineering", "BiologicalSciences", "BilogicalEng", "BiosystemsEng", "BiomedicalSciences", "BiomedicalEng", "Business Analytics", "ChemicalEngg", "Chemical&BiologicalEng", "Chemistry", "Biochemistry", "Chemistry&BioChemistry", "CS&E", "CIS", "CN", "Civil&EvnEng", "CM", "CE/CEM", "ECE", "EngMgt", "EnvEng", "ES/ESM", "Engineerg", "HA", "HCA", "IT/ITM", "INST", "IS", "INS", "IST", "INSA", "IS&M", "Industrial& SysEng", "MaterialsScience", "MaterialsEng", "Materials Science&Eng", "MML", "ManufEng/MSE", "Pharmacy", "PharmaScience", "PE", "Phy", "PH", "PM", "PA", "SE", "StructuralEng", "SystemsEng", "MPA", "MPH", "MHA", "MHCA"];

    $scope.calculateGPA = function() {
        $scope.$watch('data.percentageTextBox', function(newVal, oldVal) {
            //console.log("Search was changed to:"+newVal);
            $scope.data.gpaTextBox = (newVal / 100) * 4;
        });
    }

    $scope.getData = function() {
        console.log(requestData);
        $http({
            method: 'POST',
            url: ParseAPIUrl + '/functions/UniversitiesGraphData',
            headers: { 'Content-Type': 'application/json', 'X-Parse-Application-Id': 'DZBOXpE3uzsa4EVXIVYY45qdsVZChtIt6x8IV583', 'X-Parse-REST-API-Key': 'lUqqlkaHwpxxz7m0JIbKhm7ri5I9EGWUr1JeowfV' },
            data: requestData,
        }).success(function(responseData) {
            var xArray = [],
                yArray = [],
                valuesArray = [],
                labelsArray = [];
            $.each(responseData.result, function(key, value) {
                xArray.push(key);
                yArray.push(value);

                valuesArray.push(value);
                labelsArray.push(key);
            });
            var barData = [{
                x: xArray,
                y: yArray,
                type: 'bar'
            }];
            var pieData = [{
                values: valuesArray,
                labels: labelsArray,
                type: 'pie'
            }];

            Plotly.newPlot('barChartDiv', barData);
            Plotly.newPlot('pieChartDiv', pieData, { height: 400, width: 500 });
        });


    }
}]);