angular.module('app')
    .controller('UniversitiesController', ['$scope', '$state', 'universities', 'CloudAPI', function ($scope, $state, universities, CloudAPI) {
        $scope.universitiesByStates = universities.result;
        $scope.states = Object.keys(universities.result);
        $scope.states
            .sort(function (a, b) {
                return $scope.universitiesByStates[a].length - $scope.universitiesByStates[b].length;
            });
        $scope.addToWish = function (univ) {
            var presentStatus = univ.fav;
            univ.fav = 1;
            CloudAPI.AddToFavourite({}, {Favourite : univ.objectId}).$promise.then(function(d){
                user = d.result;
                univ.favouriteIndicator = ((user.Favourites||[]).indexOf(univ.objectId) !== -1);
                univ.fav = 2;
            }, function(){
                univ.fav = 0;
            });
        };
        $scope.replaceSpaces = function (data) {
            return data.replace(/ /g, '-');
        }
    }]);