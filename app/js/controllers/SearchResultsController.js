angular.module('app')
    .controller('SearchResultsController', ['$scope', '$state', '$stateParams', 'results', '$filter', 'CloudAPI', '$interval',
        function ($scope, $state, $stateParams, universities, $filter, CloudAPI, $interval) {
            $scope.universities = universities.result || [];
            var states = {},
                univs = {};
            for (var i = 0; i < $scope.universities.length; i++) {
                var univ = $scope.universities[i];
                states[univ.State] = 1;
                univs[univ.UniversityName] = 1;
                univ.show = true;
            }
            $scope.filters = {
                States: Object.keys(states),
                Universities: Object.keys(univs),
            };
            // {"deadLine":"Spring","course":"MS","msMajor":"CS","greScore":334,"exam":"TOEFL","examScore":120,"percentage":78}
            $scope.searchParams = $stateParams.searchParams;
            $scope.searchfilter = {
                FeeRange: [0, 100000],
                State: '',
                UniversityName: ''
            };
            $scope.changeFee = function (values) {
                $filter('searchFilter')($scope.universities, $scope.searchfilter);
            };
            $scope.filterChange = function (univs, searchFilters, active_loader) {
                $filter('searchFilter')(univs, searchFilters, active_loader);
            };
            $scope.addToWish = function (univ) {
                var presentStatus = univ.fav;
                univ.fav = 1;
                CloudAPI.AddToFavourite({}, {
                    Favourite: univ.objectId
                }).$promise.then(function (d) {
                    user = d.result;
                    univ.favouriteIndicator = ((user.Favourites || []).indexOf(univ.objectId) !== -1);
                    univ.fav = 2;
                }, function () {
                    univ.fav = 0;
                });
            };
            $scope.slider_index = 0;
            $scope.pause_sliding = 0;
            $interval(function () {
                if (!$scope.pause_sliding) {
                    $scope.slider_index = ($scope.slider_index + 1) % 5;
                }
            }, 2500);
    }]);