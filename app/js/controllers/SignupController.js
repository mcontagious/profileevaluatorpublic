angular.module('app')
    .controller('SignupController', function ($scope, $rootScope, $state) {
        $scope.signup_modal = true;
        $scope.close_modal = function () {
            $scope.signup_modal = false;
            if ($rootScope.prevState.name) {
                $state.go($rootScope.prevState.name);
            } else {
                $state.go('home');
            }
        };
        $scope.$watch('signup_modal', function () {
            if (!$scope.signup_modal) {
                $scope.close_modal();
            }
        });
    });