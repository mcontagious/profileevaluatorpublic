angular.module('app')
    .controller('BodyController', ['$scope', '$rootScope', '$localStorage', 'User', '$state', function ($scope, $rootScope, $storage, User, $state) {
        $scope.currentState = $rootScope.currentState;
        $scope.logout = function () {
            $storage.$reset();
            $scope.user = undefined;
            $state.go('home', {
                reload: true
            });
        };
        if ($storage.user) {
            User.details({
                id: $storage.user.objectId
            }, {}).$promise.then(function (data) {
                angular.extend($storage.user, data);
                $scope.user = $storage.user;
            }, function (data) {
                alertify.error(data.data.error);
            });
        }
        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {
                $scope.currentState = $rootScope.currentState = toState.name;
                $scope.toParams = $rootScope.toParams = toParams;
                $scope.user = $storage.user;
            });
    }]);