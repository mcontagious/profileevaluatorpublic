/**
 * Created by renangi on 30/12/15.
 */

(function () {
    'use strict';

    angular
        .module('app')
        .controller('userDetailsController', userDetailsController);

    userDetailsController.$inject = ['$timeout', 'CloudAPI'];

    /* @ngInject */
    function userDetailsController($timeout, API) {
        var vm = this;
        $timeout(activate);

        ////////////////

        function activate() {
            vm.details = {};
        }
        vm.find = function (params) {
            if (params.email || params.phone) {
                API.GetUserDetails(params).$promise.then(function (data) {
                    vm.details = data.result.User;
                    vm.favourites = data.result.Univs;
                }, function (err) {
                    vm.error = err;
                });
            }
        }
    }

})();