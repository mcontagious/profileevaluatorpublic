angular.module('app')
    .controller('UniversityController', ['$scope', '$rootScope', '$state', 'results', 'Universities','$localStorage',
        function ($scope, $rootScope, $state, university, Universities, $storage) {
       $scope.university = university;
       
       $scope.searchParams  = $storage.searchParams || ($storage.user || {}).LastSearchParams || {};

        $scope.isLoggedin = !!$storage.user;
        $scope.saveUniversity = function(univ) {
            Universities.saveUniversity({id : univ.objectId}, {Description : univ.Description});
        };
        
        
    }]);