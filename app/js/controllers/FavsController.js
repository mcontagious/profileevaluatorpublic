/**
 * Created by renangi on 10/11/15.
 */
angular.module('app')
    .controller('FavUniversitiesController', ['$scope', '$state', '$stateParams', 'results', '$filter', '$localStorage', 'CloudAPI',
        function ($scope, $state, $stateParams, universities, $filter, $storage, CloudAPI) {
        $scope.universities = universities.result.map(function(univ){
            univ.favouriteIndicator = true;
            return univ;
        });
        
        $scope.searchParams = $storage.searchParams;
        $scope.addToWish = function (univ) {
            var presentStatus = univ.fav;
            univ.fav = 1;
            CloudAPI.AddToFavourite({}, {Favourite : univ.objectId}).$promise.then(function(d){
                user = d.result;
                univ.favouriteIndicator = ((user.Favourites||[]).indexOf(univ.objectId) !== -1);
                univ.fav = 2;
            }, function(){
                univ.fav = 0;
            });
        };
    }]);

