/**
 * Created by renangi on 30/12/15.
 */

(function() {
    'use strict';

    angular
        .module('app')
        .controller('listOfUsersController', listOfUsersController);

    listOfUsersController.$inject = ['$scope', 'ParseAppID', 'Key', '$timeout', '$http', '$stateParams', 'ParseAPIUrl'];

    /* @ngInject */
    function listOfUsersController($scope, ParseAppId, Key, $timeout, $http, $stateParams, ParseAPIUrl) {
        $scope.searchParams = ["Backlogs", "CollegeUniversity", "CourseBuddget", "CurrentMajor", "EducationalLoan", "GREQuants", "GREVerbal", "writtenEnglishTest", "writtenGRE", "Experience", "Technology", "cgpa", "course", "deadLine", "exam", "examScore", "gpa", "greScore", "msMajor", "percentage", "yearOfpass"];
        var vm = this;
        var dateFilters = {};
        if ($stateParams.fromDate && $stateParams.toDate) {
            dateFilters = {
                where: {
                    createdAt: {
                        $gte: { __type: "Date", iso: moment($stateParams.fromDate) },
                        $lte: { __type: "Date", iso: moment($stateParams.toDate) }
                    }
                }
            }
        }
        $timeout(activate);

        ////////////////

        function activate() {
            vm.list = [];

            getUsers(0);
        }

        function getUsers(skip) {
            $http.get(ParseAPIUrl + "/users", {
                headers: {
                    "X-Parse-Application-Id": ParseAppId,
                    "X-Parse-REST-API-Key": Key
                },
                params: angular.extend({
                    limit: 1000,
                    skip: skip,
                    order: "-createdAt"
                }, dateFilters)
            }).then(function(data) {
                var x = data.data.results;

                x.splice(0, 0, x.length, 0);
                vm.list.splice.apply(vm.list, x);
                if (data.data.results.length === 1002) {
                    getUsers(skip + 1000);
                }
            })
        }
    }

})();