angular.module('app')
    .controller('HomeController', ['$scope', '$state', 'User', '$localStorage', 'usersCount',
     function ($scope, $state, User, $storage, usersCount) {
        $scope.usersCount = usersCount;
        if ($scope.user === undefined) {
            $scope.sign_in_up = 'signin';
        } else if (!$scope.user.emailVerified) {
            $scope.sign_in_up = 'verify-email';
        }
        $scope.set_sign_in_up = function (val) {
            $scope.set_sign_in_up = val;
        };
        $scope.signIn = function (fields) {
            User.signIn(fields, {}).$promise.then(function (data) {
                $storage.user = data;
                if (!$storage.user.emailVerified) {
                    $scope.showVerifyEmail = true;
                }
                alertify.success('You have been successfully loggedin to ProfileEvaluator');
                $state.go('PES');
            }, function (data) {
                alertify.error(data.data.error);
            });
        };
        $scope.signUp = function (fields, searchParams) {
            AccountKit.login('PHONE', {
                countryCode: '+91',
                phoneNumber: fields.phone
            }, function(response) {
                 if (response.status === "PARTIALLY_AUTHENTICATED") {
                    fields.email = fields.username;
                    fields.phoneVerified = true;
                    User.signUp(fields).$promise.then(function (data) {
                        $storage.user = data;
                        $scope.sign_in_up = 'verify-email';
                        alertify.success('You have been successfully signedup on ProfileEvaluator');
                    }, function (data) {
                        alertify.error(data.data.error);
                    });
                 } else {
                     alertify.error("Mobile verification failed. We have mandated mobile verification to avoid misuse of this application.");
                 }
            });
        };
        $scope.facebookSignInUp = function () {
            Parse.FacebookUtils.logIn("public_profile,email", {
                success: function (user) {
                    if (!user.existed()) {
                        alertify.success("User signed up and logged in through Facebook!");
                    } else {
                        alertify.success("User logged in through Facebook!");
                    }
                    $storage.user = data;
                    $state.go('PES');
                },
                error: function (user, error) {
                    alertify.error("User cancelled the Facebook login or did not fully authorize.");
                }
            }, {
                scope: 'email'
            });
        };
    }]);