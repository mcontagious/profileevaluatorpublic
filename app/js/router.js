angular.module('app')
    .config(['$locationProvider', '$stateProvider', '$urlRouterProvider', '$httpProvider', 'ParseAppID', 'ParseJSKey', 'ParseAPIUrl',
        function($locationProvider, $stateProvider, $urlRouterProvider, $httpProvider, ParseAppID, ParseJSKey, ParseAPIUrl) {
            Parse.serverURL = ParseAPIUrl;
            Parse.initialize(ParseAppID);
            $locationProvider.html5Mode(true).hashPrefix('!');
            //        $httpProvider.defaults.withCredentials = true;
            $httpProvider.defaults.useXDomain = true;
            $httpProvider.defaults.headers.patch = {
                'Content-Type': 'application/json;charset=utf-8'
            };
            $httpProvider.defaults.headers.post = {
                'Content-Type': 'application/json;charset=utf-8'
            };

            var checkLoggedIn = ['$q', '$timeout', '$state', '$localStorage', function($q, $timeout, $state, $storage) {
                var deferred = $q.defer();
                $timeout(function() {
                    if (!!$storage.user) {
                        console.log('Logged in');
                        $timeout(deferred.resolve);
                    } else {
                        $timeout(deferred.reject);
                        $state.go('home');
                    }
                });
                return deferred.promise;
            }];

            $urlRouterProvider.otherwise("/");
            $stateProvider.state("error", {
                url: "/error",
                templateUrl: 'error.html'
            });

            $stateProvider.state("home", {
                url: "/",
                templateUrl: 'home.html',
                controller: 'HomeController',
                resolve: {
                    usersCount: ['$q', '$timeout', function($q, $timeout) {
                        var deferred = $q.defer();
                        $timeout(function() {
                            var q = new Parse.Query(Parse.User);
                            return q.count().then(function(count) {
                                return deferred.resolve(5904 + count);
                            }, function(err) {
                                console.log(err);
                                return deferred.resolve(15531);
                            })
                        });
                        return deferred.promise;
                    }]
                }

            });
            $stateProvider.state("aboutus", {
                url: "/about-us",
                templateUrl: 'about-us.html',
                controller: 'AboutUsController'
            });
            $stateProvider.state("ceo", {
                url: "/about-us/ceo",
                templateUrl: 'ceo.html'
            });
            $stateProvider.state("trainer", {
                url: "/about-us/trainer",
                templateUrl: 'trainer.html'
            });
            $stateProvider.state("fcounsellor", {
                url: "/about-us/counsellor",
                templateUrl: 'fcounsellor.html'
            });
            $stateProvider.state("scounsellor", {
                url: "/about-us/counsellor",
                templateUrl: 'scounsellor.html'
            });
            $stateProvider.state("services", {
                url: "/services",
                templateUrl: 'services.html',
                controller: 'ServicesController'
            });
            $stateProvider.state("terms", {
                url: "/terms",
                templateUrl: 'terms.html',
                controller: 'TermsController'
            });
            $stateProvider.state("contactus", {
                url: "/contact-us",
                templateUrl: 'contact-us.html',
                controller: 'ContactUsController'
            });

            $stateProvider.state("pricing", {
                url: "/pricing",
                templateUrl: 'pricing.html',
                controller: 'pricingController'
            });
            $stateProvider.state("privacypolicy", {
                url: "/privacypolicy",
                templateUrl: 'privacypolicy.html',
                controller: 'privacypolicyController'
            });

            $stateProvider.state("blogs", {
                url: "/blogs",
                templateUrl: 'blogs.html',
                controller: 'pricingController'
            });

            $stateProvider.state("searchcriteria", {
                url: "/searchcriteria",
                templateUrl: 'searchcriteria.html',
                controller: 'SearchCriteriaController'
            });
            $stateProvider.state("LOUnivs", {
                url: "/List-Of-Universities",
                templateUrl: 'universities.html',
                controller: 'UniversitiesController',
                loggedIn: checkLoggedIn,
                resolve: {
                    
                    universities: ['CloudAPI', '$q', '$timeout', function(CloudAPI, $q, $timeout) {
                        var deferred = $q.defer();
                        $timeout(function() {
                            CloudAPI.UniversitiesList().$promise.then(deferred.resolve, deferred.reject);
                        });
                        return deferred.promise;
                    }]
                }
            });


            $stateProvider.state("PES", {
                url: "/Profile-Evaluator",
                templateUrl: 'evaluator.html',
                controller: 'ProfileEvaluatorSearchController',
                params: {
                    searchParams: null
                },
                resolve: {
                    searchParams: ['$stateParams', '$localStorage', function($stateParams, $storage) {
                        return $storage.user.LastSearchParams || {};
                    }],
                    IP: ['$http', function($http) {
                        return $http.get("//ipfind.co/me?auth=4e17d1da-9812-433c-9f12-83aca0f98d5c");
                    }]
                }
            });

            $stateProvider.state("searchdb", {
                url: "/search-results",
                templateUrl: 'search-results.html',
                controller: 'SearchResultsController',
                params: {
                    searchParams: null
                },
                resolve: {
                    results: ['$q', '$stateParams', '$state', '$timeout', 'CloudAPI', '$localStorage', function($q, $stateParams, $state, $timeout, CloudAPI, $storage) {
                        var deffered = $q.defer();
                        $timeout(function() {
                            $stateParams.searchParams = $stateParams.searchParams || $storage.searchParams;
                            $storage.searchParams = $stateParams.searchParams;
                            $storage.user = angular.extend($storage.user, {
                                LastSearchParams: $stateParams.searchParams
                            });
                            if ($stateParams.searchParams === null) {
                                $state.go('home');
                                deffered.reject(null);
                            } else {
                                CloudAPI.SearchUniversities($stateParams.searchParams).$promise.then(deffered.resolve, deffered.rejected);
                            }
                        });
                        return deffered.promise;
                    }]
                }
            });

            $stateProvider.state("profile", {
                url: "/profile",
                templateUrl: 'profile.html',
                controller: 'ProfileController',
                params: {
                    searchParams: null
                },
                resolve: {
                    results: ['$q', '$stateParams', '$state', '$timeout', 'CloudAPI', '$localStorage', function($q, $stateParams, $state, $timeout, CloudAPI, $storage) {
                        var deffered = $q.defer();
                        $timeout(function() {
                            $stateParams.searchParams = $stateParams.searchParams || $storage.searchParams;
                            $storage.searchParams = $stateParams.searchParams;
                            $storage.user = angular.extend($storage.user, {
                                LastSearchParams: $stateParams.searchParams
                            });
                            if ($stateParams.searchParams === null) {
                                $state.go('home');
                                deffered.reject(null);
                            } else {
                                CloudAPI.SearchUniversities($stateParams.searchParams).$promise.then(deffered.resolve, deffered.rejected);
                            }
                        });
                        return deffered.promise;
                    }]
                }
            });

            $stateProvider.state("favUnivs", {
                url: "/favourites",
                templateUrl: 'favourites.html',
                controller: 'FavUniversitiesController',
                resolve: {
                    results: ['$q', '$stateParams', '$state', '$timeout', 'CloudAPI', function($q, $stateParams, $state, $timeout, CloudAPI) {
                        var deffered = $q.defer();
                        $timeout(function() {
                            CloudAPI.GetFavUniversities().$promise.then(deffered.resolve, deffered.rejected);
                        });
                        return deffered.promise;
                    }]
                }
            });
            $stateProvider.state("university", {
                url: "/university/:id",
                templateUrl: 'university.html',
                controller: 'UniversityController',
                resolve: {
                    results: ['$q', '$stateParams', '$state', '$timeout', 'Universities', function($q, $stateParams, $state, $timeout, Universities) {
                        var deffered = $q.defer();
                        $timeout(function() {
                            if ($stateParams.id === null) {
                                $state.go('home');
                                deffered.reject(null);
                            } else {
                                Universities.getByID({
                                    id: $stateParams.id
                                }).$promise.then(deffered.resolve, deffered.rejected);
                            }
                        });
                        return deffered.promise;
                    }]
                }
            });

            $stateProvider.state("universitybydetails", {
                url: "/university/name/:name/location/:location/state/:state",
                templateUrl: 'university.html',
                controller: 'UniversityController',
                resolve: {
                    results: ['$q', '$stateParams', '$state', '$timeout', 'Universities', function($q, $stateParams, $state, $timeout, Universities) {
                        var deffered = $q.defer();
                        $timeout(function() {
                            if ($stateParams.id === null) {
                                $state.go('home');
                                deffered.reject(null);
                            } else {
                                // http://localhost:8000/university/name/University-of-Vermont/location/Burlington/state/VERMONT
                                Universities.getByNameLocationAndState({
                                    where: {
                                        UniversityName: $stateParams.name.replace(/\-/g, ' '),
                                        Place: $stateParams.location.replace(/\-/g, ' '),
                                        State: $stateParams.state.replace(/\-/g, ' ')
                                    },
                                    limit: 1
                                }).$promise.then(function(data) {
                                    if (data.results[0]) {
                                        deffered.resolve(data.results[0]);
                                    } else {
                                        deffered.rejected(new Error('University not found with that data.'));
                                    }
                                }, deffered.rejected);
                            }
                        });
                        return deffered.promise;
                    }]
                }
            });
            $stateProvider.state("list-of-users", {
                url: "/list-of-users/:key",
                templateUrl: 'list-of-users.html',
                controller: 'listOfUsersController',
                controllerAs: 'Users',
                resolve: {
                    Key: ['$stateParams', function($stateParams) {
                        return $stateParams.key;
                    }]
                }
            });
            $stateProvider.state("list-of-users-by-date", {
                url: "/list-of-users/:key/from/:fromDate/to/:toDate",
                templateUrl: 'list-of-users.html',
                controller: 'listOfUsersController',
                controllerAs: 'Users',
                resolve: {
                    Key: ['$stateParams', function($stateParams) {
                        return $stateParams.key;
                    }]
                }
            });

            $stateProvider.state("list-of-users-visits-by-date", {
                url: "/list-of-users-visits/:key/from/:fromDate/to/:toDate",
                templateUrl: 'list-of-users-visits.html',
                controller: 'listOfUserVisitsController',
                controllerAs: 'UserVisits',
                resolve: {
                    Key: ['$stateParams', function($stateParams) {
                        return $stateParams.key;
                    }]
                }
            });

            $stateProvider.state("user-details", {
                url: "/2801636ac65d840f1735dc0833a61b69",
                templateUrl: 'user-details.html',
                controller: 'userDetailsController',
                controllerAs: 'User',
                resolve: {
                    loggedIn: checkLoggedIn
                }
            });
            $stateProvider.state("login", {
                url: "/login",
                templateUrl: 'login.html',
                controller: 'LoginController'
            });
            $stateProvider.state("signup", {
                url: "/signup",
                templateUrl: 'signup.html',
                controller: 'SignupController'
            });

        }
    ]);