angular.deep_copy = function (obj) {
    return JSON.parse(JSON.stringify(obj));
};

angular.module('app', ['ngResource', 'angularify.semantic', "ui.router", 'ngFx', 'ngAnimate', 'ngLocale', 'ngStorage', 'ngSanitize', 'infinite-scroll', 'angular-loading-bar'])
    .run(function ($rootScope) {
       L.mapbox.accessToken = 'pk.eyJ1IjoibWNvbnRhZ2lvdXMiLCJhIjoiY2lmM3QwcHF3MnlhdnN1bTNzbGt3dGtpcCJ9.-ibR-trVwNs9Ix4CWhXSLg';
        $rootScope.$on('$stateChangeSuccess', function (ev, to, toParams, from, fromParams) {
            $rootScope.prevState = from;
        });
    });