// jshint ignore: start
(function () {
    'use strict';

    angular
        .module('app')
        .filter('searchFilter', searchFilter);

    searchFilter.$inject = ['$timeout'];

    function searchFilter($timeout) {

        return searchFilterFilter;

        ////////////////

        function searchFilterFilter(univs, filters, indicator) {
            indicator = true;
            $timeout(function () {
                if (filters === undefined) {
                    return;
                }
                var i = 0,
                    j = 0,
                    filtersKeys = Object.keys(filters),
                    univ = {};
                for (i = 0; i < univs.length; i++) {
                    univ = univs[i];
                    univ.show = true;
                    for (j = 0; j < filtersKeys.length; j++) {
                        var FilterKey = filtersKeys[j];
                        if (FilterKey === 'FeeRange') {
                            univ.show = (univ.TutionFee >= filters[FilterKey][0] && univ.TutionFee <= filters[FilterKey][1]);
                        } else if (angular.isArray(filters[FilterKey]) && filters[FilterKey].length > 0 && univ[FilterKey] && filters[FilterKey].indexOf(univ[FilterKey]) === -1) {
                            univ.show = false;
                        } else if (!angular.isArray(filters[FilterKey]) && !!filters[FilterKey] && univ[FilterKey] && filters[FilterKey] !== univ[FilterKey]) {
                            univ.show = false;
                        }
                    }
                }
                indicator = false;
            });
            return univs;
        }
    }
})();