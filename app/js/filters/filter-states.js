(function () {
    'use strict';
    angular
        .module('app')
        .filter('filterStates', filterStates);

    filterStates.$inject = ['$filter'];

    function filterStates($filter) {
        return filterFilter;
        ////////////////

        function filterFilter(states, filterOptions, univs) {
            if (!filterOptions) {
                return states;
            } else {
                return states.filter(function (state) {
                    var filt = $filter('filter')(univs[state], filterOptions);
                    return (filt.length > 0);
                });
            }
        }
    }
})();