angular.module('app')
    .directive('status', [function () {
        return {
            restrict: 'A',
            scope : {
                status : "="
            },
            link: function ($scope, $element, $attrs) {
                $scope.$watch('status', function(){
                    if ($scope.status !== undefined) {
                        $element.popup({
                            inline: true,
                            hoverable: true,
                            position: 'bottom center',
                            on: 'click',                         
                            title   : 'Popup Title srinivas',
                            content : 'Green–Safe  Orange–Ambitious  Red-Moderate ',
                            delay: {
                                show: 300,
                                hide: 800
                            }
                        })
                    }
                })
            }
        };
}]);