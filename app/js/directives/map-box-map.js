angular.module('app')
    .directive('mapBoxMap', function () {
        return {
            restrict: 'A',
            scope: {
                lat: '=',
                lng: '=',
                mapId: '=',
                zoomLevel: '=',
                markers: '='
            },
            link: function ($scope, $element, $attrs) {
                $scope.map = L.mapbox.map($element[0], $scope.mapId)
                    .setView([$scope.lat, $scope.lng], $scope.zoomLevel);
                if (!!$scope.markers) {
                    $scope.markers.forEach(function (marker) {
                        L.marker(marker, {
                            icon: L.mapbox.marker.icon({
                                'marker-size': 'medium',
                                'marker-symbol': 'town-hall',
                                'marker-color': '#fa0'
                            })
                        }).addTo($scope.map);
                    });
                }
            }
        };
    });