angular.module('app')
    .directive('modeel', [function () {
        return {
            restrict: 'A',
            scope: {
                modeel: "="
            },
            link: function ($scope, $element, $attrs) {
                $element.on('click', function(){
                    jQuery('.ui.small.modal')
                    .modal('show');
                });
            }
        };
}]);