angular.module('app')
    .directive('rating', [function () {
        return {
            restrict: 'C',
            scope: {
                rating: '=ratings',
                maxRating: '=',
                readonly: '='
            },
            link: function ($scope, $element, $attrs) {
                $element
                    .rating($scope.readonly ? 'disable' : 'enable');
                $scope.$watch('rating', function () {
                    if ($scope.rating) {
                        $element.rating('set rating', $scope.rating);
                    }
                });
            }
        };
    }]);