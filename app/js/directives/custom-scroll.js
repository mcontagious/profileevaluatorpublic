angular.module('app')
    .directive('customScroll', ['$timeout', function ($timeout) {
        return {
            restrict: 'AC',
            scope: {
                theme: '=',
                axis: '='
            },
            link: function (scope, element, attrs) {
                $timeout(function () {
                    element.mCustomScrollbar({
                        theme: scope.theme || 'minimal-dark',
                        axis: scope.axis
                    });
                });
            }
        };
}]);