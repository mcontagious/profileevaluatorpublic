angular.module('app')
    .directive('studentRequestForm', function () {
        return {
            restrict: 'E',
            templateUrl: "student-request-form.html",
            controller: ['$scope', 'CloudAPI', function($scope, CloudAPI) { 
                $scope.request = {
                    name: "Chaitanya",
                    email: "",
                    phoneNumber:"",
                    serviceType: ""
                };
                $scope.message = "";
                $scope.onFormSubmit = function () {
                    CloudAPI.SOPRequest($scope.request).$promise.then(function(results) {
                        $scope.message = $scope.request.serviceType + " request has been created. We will get back to you ASAP."
                    }, function(err) {
                        console.error(err);
                        alert(err.message);
                    });
                };
            }]
        };
});