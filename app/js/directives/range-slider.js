// jshint ignore: start
(function () {
    'use strict';

    angular
        .module('app')
        .directive('rangeSlider', directive);

    directive.$inject = [];

    /* @ngInject */
    function directive() {
        // Usage:
        //
        // Creates:
        //
        var directive = {
            link: link,
            restrict: 'A',
            require: 'ngModel',
            scope: {
                change: '&onChange'
            }
        };
        return directive;

        function link(scope, element, attrs, ngModel) {
            element.slider({
                range: true,
                min: 0,
                max: 100000,
                step: 1000,
                values: [0, 100000],
                slide: function (event, ui) {
                    ngModel.$setViewValue(ui.values);
                    scope.change({
                        values: ui.values
                    });
                }
            });
        }
    }
})();