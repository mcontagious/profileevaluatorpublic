angular.module('app')
    .directive('sideMenu', [function () {
        return {
            restrict: 'A',
            link: function ($scope, $element, $attrs) {
                $element.on("click", function (e) {
                    e.preventDefault();
                    $('.ui.vertical.menu').toggle();
                });
            }
        };
}]);