angular.module('app')
    .directive('dropdown', ['$timeout', function ($timeout) {
        return {
            restrict: 'AC',
            require: 'ngModel',
            link: function ($scope, $element, $attrs, $ngModel) {
                var value = '';
                if ($attrs.multiple !== undefined) {
                    value = [];
                }
                $element.dropdown({
                    onAdd: function (val, text, $selectedItem) {
                        $timeout(function () {
                            if ($attrs.multiple !== undefined) {
                                value.push(text);
                                $ngModel.$setViewValue(value);
                            } else {
                                $ngModel.$setViewValue(text);
                            }
                        });
                    },
                    onRemove: function (val, text, $selectedItem) {
                        $timeout(function () {
                            if ($attrs.multiple !== undefined) {
                                var ind = value.indexOf(text);
                                if (ind !== -1) {
                                    value.splice(ind, 1);
                                }
                                $ngModel.$setViewValue(value);
                            } else {
                                $ngModel.$setViewValue('');
                            }
                        });
                    }
                });
                $ngModel.$render = function () {
                    $timeout(function () {
                        $element.dropdown('set selected', $ngModel.$viewValue || '');
                    });
                };
            }
        };
}]);