/* Exports a function which returns an object that overrides the default &
 *   plugin file patterns (used widely through the app configuration)
 *
 * To see the default definitions for Lineman's file paths and globs, see:
 *
 *   - https://github.com/linemanjs/lineman/blob/master/config/files.coffee
 */
module.exports = function(lineman) {
  //Override file patterns here
  return {
   ngtemplates: {
		dest: "generated/angular/template-cache.js"
	},
  	webfonts: {
		files: {
			"webfonts": "vendor/webfonts/*.*"
		},
		root : "css"
	},
    js: {
      vendor: [
        "vendor/js/jquery.js",
        "vendor/js/TweenMax.js",
        "vendor/js/highstock.js",
        "vendor/js/angular.js",
        "vendor/js/angular-locale_en-in.js",
        "vendor/js/moment.js",
        "vendor/js/**/*.js"
      ],
      app: [
        "app/js/app.js",
        "app/js/**/*.js"
      ]
    },
    less: {
      compile: {
        options: {
          paths: ["vendor/css/**/*.css", "app/css/**/*.less"]
        }
      }
    }
  };
};
